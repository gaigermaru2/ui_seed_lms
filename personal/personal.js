import "../vendors/@progress/kendo-ui/2020.3.1118/js/kendo.all.min.js"

document.addEventListener("DOMContentLoaded", function (event) {
    //initial section
    setTimeout(function () {
        document.body.classList.remove('c-no-layout-transition')
    }, 2000);
   // alert(currentLang);
    createCookie("current_language", currentLang, 30);
    fetchI18nLabel(i18nUrl, currentLang);
    dropdownLangBinding(i18nUrl, currentLang);
    //initial kendoui component ******
    $('#grid').kendoGrid({
        height: 550,
        sortable: true
    });
    $('#txt_name').kendoAutoComplete(["kongsak", "saluda"]);
    $('#txt_lastname').kendoTextBox();
    $('#txt_age').kendoNumericTextBox({
        min: 15,
        max: 200,
        step: 1,
        value: 15,
        format: "n0",
    });
    //*** initial data grid
    $('#grd_personal').kendoGrid({
        scrollable: true,
        sortable: {mode: 'single'},
        filterable: false,
        pageable: {
            refresh: true,
            input: true,
            numeric: false,
            pageSizes: [10, 20, 50]
        },
        dataSource: {
            transport: {
                read:function (options) {

                    fetch("../datasets/personal/personal_grid.json", {
                        method: 'POST',
                        cache: 'no-cache',
                        headers: {
                            'Content-Type': 'application/json'
                            // 'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: JSON.stringify({})
                    }).then(response => response.json())
                        .then(responseObj => {
                            options.success(responseObj);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }
            },
            sort: [
                {field: 'name', dir: 'desc'},
            ],
            serverSorting: true,
            serverFiltering: true,
            serverPaging: false,
            pageSize: 10,
            schema: {
                data: 'datas',
                total: 'total',
                model: {
                    fields: {
                        "name": {type: "string"},
                        "last_name": {type: "string"},
                        "age": {type: "number"},
                        "birth_date": {type: "number"}
                    }
                },
            },
        },
        columns: [
            {
                selectable: true,
                headerAttributes: {
                    class: "text-center",
                },
                attributes: {class: "text-center"},
                width: "3em",
            },
            {
                field: "name",
                title: "Name",
                headerAttributes: {
                    class: "text-center font-weight-bold",
                },
                attributes: {class: "text-left"},
                width: "12em",
            },
            {
                field: "last_name",
                title: "Last Name",
                headerAttributes: {
                    class: "text-center font-weight-bold"
                },
                attributes: {class: "text-left"},
                width: "16em",
            },
            {
                field: "age",
                title: "Age",
                headerAttributes: {
                    class: "text-center font-weight-bold"
                },
                attributes: {class: "text-right"},
                width: "5em",
            },
            {
                field: "birth_date",
                title: "Birth Date",
                headerAttributes: {
                    class: "text-center font-weight-bold"
                },
                attributes: {class: "text-center"},
                template: "#= kendo.toString(new Date(Number(birth_date)), 'dd/MM/yyyy') #",
                width: "10em",
            }

        ],
    });

    const content = document.getElementById("content");
    content.style.display = "";
    const footer = document.getElementById("footer");
    footer.style.display = "";
});




//import "../vendors/@progress/kendo-ui/2020.3.1118/js/kendo.all.min.js"

$(() => {
        $('#grid').kendoGrid({
            height: 550,
            sortable: true
        });

        $('#name').kendoAutoComplete(["kongsak", "saluda"]);
    }
);


$(function() {
    $("#scheduler").kendoScheduler({
        date: new Date("2021/2/2"),
        startTime: new Date("2021/2/2 07:00 AM"),
        height: 500,
        views: [
        //"day",
        { type: "month", selected: true },
        "week",
        "month",
        "agenda",
        //{ type: "timeline", eventHeight: 50}
        ],
        timezone: "Etc/UTC",
        dataSource: {
            batch: true,
            transport: {
                read: {
                    url: "https://demos.telerik.com/kendo-ui/service/tasks",
                    dataType: "jsonp"
                },
                update: {
                    url: "https://demos.telerik.com/kendo-ui/service/tasks/update",
                    dataType: "jsonp"
                },
                create: {
                    url: "https://demos.telerik.com/kendo-ui/service/tasks/create",
                    dataType: "jsonp"
                },
                destroy: {
                    url: "https://demos.telerik.com/kendo-ui/service/tasks/destroy",
                    dataType: "jsonp"
                },
                parameterMap: function(options, operation) {
                    if (operation !== "read" && options.models) {
                        return {models: kendo.stringify(options.models)};
                    }
                }
            },
            schema: {
                model: {
                    id: "taskId",
                    fields: {
                        taskId: { from: "TaskID", type: "number" },
                        title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                        start: { type: "date", from: "Start" },
                        end: { type: "date", from: "End" },
                        startTimezone: { from: "StartTimezone" },
                        endTimezone: { from: "EndTimezone" },
                        description: { from: "Description" },
                        recurrenceId: { from: "RecurrenceID" },
                        recurrenceRule: { from: "RecurrenceRule" },
                        recurrenceException: { from: "RecurrenceException" },
                        ownerId: { from: "OwnerID", defaultValue: 1 },
                        isAllDay: { type: "boolean", from: "IsAllDay" }
                    }
                }
            },
            filter: {
                logic: "or",
                filters: [
                { field: "ownerId", operator: "eq", value: 1 },
                { field: "ownerId", operator: "eq", value: 2 },
                { field: "ownerId", operator: "eq", value: 3 }
                ]
            }
        },
        resources: [
        {
            field: "ownerId",
            title: "Owner",
            dataSource: [
            { text: "Alex", value: 1, color: "#2eb85c" },
            { text: "Bob", value: 2, color: "#2eb85c" },
            { text: "Charlie", value: 3, color: "#2eb85c" }
            ]
        }
        ]
    });

    $("#people :checkbox").change(function(e) {
        var checked = $.map($("#people :checked"), function(checkbox) {
            return parseInt($(checkbox).val());
        });

        var scheduler = $("#scheduler").data("kendoScheduler");

        scheduler.dataSource.filter({
            operator: function(task) {
                return $.inArray(task.ownerId, checked) >= 0;
            }
        });
    });
});
  






        function createChartOverall() {
            $("#chartOverall").kendoChart({
                title: {
                    text: "Overall Performance"
                },
                legend: {
                    position: "bottom"
                },
                chartArea: {
                    background: ""
                },
                seriesDefaults: {
                    type: "line",
                    style: "smooth"
                },
                series: [{
                    name: "India",
                    data: [3.907, 7.943, 7.848, 9.284]
                },{
                    name: "World",
                    data: [1.988, 2.733, 3.994, 3.464]
                },{
                    name: "Russian Federation",
                    data: [4.743, 7.295, 7.175, 6.376]
                },{
                    name: "Haiti",
                    data: [-0.253, 0.362, -3.519, 1.799]
                }],
                valueAxis: {
                    labels: {
                        format: "{0}%"
                    },
                    line: {
                        visible: false
                    },
                    axisCrossingValue: -10
                },
                categoryAxis: {
                    categories: [2002, 2003, 2004, 2005],
                    majorGridLines: {
                        visible: false
                    },
                    labels: {
                        rotation: "auto"
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0}%",
                    template: "#= series.name #: #= value #"
                }
            });
        }
        function createChartMind() {
            $("#chartMindset").kendoChart({
                title: {
                    text: "Overall Mindset"
                },
                legend: {
                    position: "top"
                },
                chartArea: {
                    background: ""
                },
                seriesDefaults: {
                    type: "line",
                    style: "smooth"
                },
                series: [{
                    name: "India",
                    data: [3.907, 7.943, 7.848, 9.284]
                },{
                    name: "World",
                    data: [1.988, 2.733, 3.994, 3.464]
                },],
                valueAxis: {
                    labels: {
                        format: "{0}%"
                    },
                    line: {
                        visible: false
                    },
                    axisCrossingValue: -10
                },
                categoryAxis: {
                    categories: [2002, 2003, 2004, 2005],
                    majorGridLines: {
                        visible: false
                    },
                    labels: {
                        rotation: "auto"
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0}%",
                    template: "#= series.name #: #= value #"
                }
            });
        }

        $(document).ready(createChartOverall);
        $(document).ready(createChartMind);
        
        //$(document).bind("kendo:skinChange", createChartMindset);
        $(document).bind("kendo:skinChange", createChartOverall);
        $(document).bind("kendo:skinChange", createChartMind);
