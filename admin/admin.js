import "../vendors/@progress/kendo-ui/2020.3.1118/js/kendo.all.min.js"

document.addEventListener("DOMContentLoaded", function (event) {
    //initial section
    setTimeout(function () {
        document.body.classList.remove('c-no-layout-transition')
    }, 2000);
   // alert(currentLang);
   createCookie("current_language", currentLang, 30);
   fetchI18nLabel(i18nUrl, currentLang);
   dropdownLangBinding(i18nUrl, currentLang);
    //initial kendoui component ******
    $('#grid').kendoGrid({
        height: 550,
        sortable: true
    });
    $('#txt_company').kendoTextBox();
    $('#txt_companyid').kendoTextBox();
    
    //*** initial data grid
    $('#grd_company').kendoGrid({
        scrollable: true,
        sortable: {mode: 'single'},
        filterable: false,
        pageable: {
            refresh: true,
            input: true,
            numeric: false,
            pageSizes: [10, 20, 50]
        },
        dataSource: {
            transport: {
                read:
                {
                    //url: "http://localhost:9999/personal/datatable.json",
                    url: "../datasets/company/company_grid.json",//crudServiceBaseUrl + "/detailproducts""",
                    dataType: "json"
                }
                /*function (options) {

                    fetch("../datasets/company/company_grid.json", {
                        method: 'POST',
                        cache: 'no-cache',

                        headers: {
                            'Content-Type': 'application/json'

                            // 'Content-Type': 'application/x-www-form-urlencoded'

                        },
                        body: JSON.stringify({})
                    }).then(response => response.json())
                    .then(responseObj => {
                        options.success(responseObj);
                    })
                    .catch(err => {
                           // alert(err);
                           console.log(err);
                       });
                }*/
            },
            sort: [
            {field: 'company_id', dir: 'company_name'},
            ],
            serverSorting: true,
            serverFiltering: true,
            serverPaging: false,
            pageSize: 10,
            schema: {
                data: 'datas',
                total: 'total',
                model: {
                    fields: {
                        "company_name": {type: "string"},
                        "company_id": {type: "string"}
                    }
                },
            },
        },
        columns: [
        {
            selectable: true,
            headerAttributes: {
                class: "text-center",
            },
            attributes: {class: "text-center"},
            width: "3em",
        },
        {
            field: "company_name",
            title: "Name",
            headerAttributes: {
                class: "text-center font-weight-bold",
            },
            attributes: {class: "text-left"},
            width: "8em",
        },
        {
            field: "company_id",
            title: "ID",
            headerAttributes: {
                class: "text-center font-weight-bold"
            },
            attributes: {class: "text-left"},
            width: "4em",
        },
       
        { command: ["edit","destroy"], title: "&nbsp;", width: "200px" },
        ],editable: "popup"
    });

    const content = document.getElementById("content");
    content.style.display = "";
    const footer = document.getElementById("footer");
    footer.style.display = "";
});






$(document).ready(function() {
    var today = new Date(),
    events = [
    +new Date(today.getFullYear(), today.getMonth(), 8),
    +new Date(today.getFullYear(), today.getMonth(), 12),
    +new Date(today.getFullYear(), today.getMonth(), 24),
    +new Date(today.getFullYear(), today.getMonth() + 1, 6),
    +new Date(today.getFullYear(), today.getMonth() + 1, 7),
    +new Date(today.getFullYear(), today.getMonth() + 1, 25),
    +new Date(today.getFullYear(), today.getMonth() + 1, 27),
    +new Date(today.getFullYear(), today.getMonth() - 1, 3),
    +new Date(today.getFullYear(), today.getMonth() - 1, 5),
    +new Date(today.getFullYear(), today.getMonth() - 2, 22),
    +new Date(today.getFullYear(), today.getMonth() - 2, 27)
    ];

    $("#calendar").kendoCalendar({
        value: today,
        dates: events,
        weekNumber: true,
        month: {
                        // template for dates in month view
                        content: '# if ($.inArray(+data.date, data.dates) != -1) { #' +
                        '<div class="' +
                        '# if (data.value < 10) { #' +
                        "exhibition" +
                        '# } else if ( data.value < 20 ) { #' +
                        "party" +
                        '# } else { #' +
                        "cocktail" +
                        '# } #' +
                        '">#= data.value #</div>' +
                        '# } else { #' +
                        '#= data.value #' +
                        '# } #',
                        weekNumber: '<a class="italic">#= data.weekNumber #</a>'
                    },
                    footer: false
                });
});







function createChartOverall() {
    $("#chartOverall").kendoChart({
        title: {
            text: "Overall Performance"
        },
        legend: {
            position: "top"
        },
        chartArea: {
            background: "",
            height:250
        },
        seriesDefaults: {
            type: "line",
            style: "smooth"
        },
        series: [{
            name: "India",
            data: [3.907, 7.943, 7.848, 9.284]
        },{
            name: "World",
            data: [1.988, 2.733, 3.994, 3.464]
        },{
            name: "Russian Federation",
            data: [4.743, 7.295, 7.175, 6.376]
        },{
            name: "Haiti",
            data: [-0.253, 0.362, -3.519, 1.799]
        }],
        valueAxis: {
            labels: {
                format: "{0}%"
            },
            line: {
                visible: false
            },
            axisCrossingValue: -10
        },
        categoryAxis: {
            categories: [2002, 2003, 2004, 2005],
            majorGridLines: {
                visible: false
            },
            labels: {
                rotation: "auto"
            }
        },
        tooltip: {
            visible: true,
            format: "{0}%",
            template: "#= series.name #: #= value #"
        }
    });
}
function createChartMind() {
    $("#chartMindset").kendoChart({
        title: {
            text: "Overall Mindset"
        },
        legend: {
            position: "top"
        },
        chartArea: {
            background: "",
            height:250
        },
        seriesDefaults: {
            type: "line",
            style: "smooth"
        },
        series: [{
            name: "India",
            data: [3.907, 7.943, 7.848, 9.284]
        },{
            name: "World",
            data: [1.988, 2.733, 3.994, 3.464]
        },],
        valueAxis: {
            labels: {
                format: "{0}%"
            },
            line: {
                visible: false
            },
            axisCrossingValue: -10
        },
        categoryAxis: {
            categories: [2002, 2003, 2004, 2005],
            majorGridLines: {
                visible: false
            },
            labels: {
                rotation: "auto"
            }
        },
        tooltip: {
            visible: true,
            format: "{0}%",
            template: "#= series.name #: #= value #"
        }
    });
}

$(document).ready(createChartOverall);
$(document).ready(createChartMind);

        //$(document).bind("kendo:skinChange", createChartMindset);
        $(document).bind("kendo:skinChange", createChartOverall);
        $(document).bind("kendo:skinChange", createChartMind);
