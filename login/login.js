import "../vendors/@progress/kendo-ui/2020.3.1118/js/kendo.all.min.js"

document.addEventListener("DOMContentLoaded", function (event) {
    //initial section
    setTimeout(function () {
        document.body.classList.remove('c-no-layout-transition')
    }, 2000);

    createCookie("current_language", currentLang, 30);
    fetchI18nLabel(i18nUrl, currentLang);
    dropdownLangBinding(i18nUrl, currentLang);

    //initial kendoui component ******
    
});
