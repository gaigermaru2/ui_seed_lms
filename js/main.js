function fetchI18nLabel(url, lang) {
    let i18nUrl = `${url}/${lang}.json`
    fetch(i18nUrl, {
        cache: 'no-cache'
    })
        .then(response => response.json())
        .then(({labels, placeholders, kendoGridHeaders}) => {
            if (labels) {
                const elementIds = Object.keys(labels)
                for (const elementId of elementIds) {
                    const element = document.getElementById(elementId);
                    if (element) {
                        element.innerText = labels[elementId]
                    }
                }
            }
            if (placeholders) {
                const elementIds = Object.keys(placeholders);
                for (const elementId of elementIds) {
                    const element = document.getElementById(elementId);
                    if (element) {
                        element.placeholder = placeholders[elementId]
                    }
                }
            }
            if (kendoGridHeaders) {
                const gridIds = Object.keys(kendoGridHeaders);
                for (const gridId of gridIds) {
                    const fieldNames = Object.keys(kendoGridHeaders[gridId]);
                    for (const fieldName of fieldNames) {
                        const position = `#${gridId} thead [data-field=${fieldName}] .k-link`
                        const value = kendoGridHeaders[gridId][fieldName];
                        $(position).html(value);
                    }
                }
            }
        })
        .catch(error => console.log("", error));
}

function dropdownLangBinding(i18nUrl, lang) {
    const languageSettingUrl = '../settings/language_selection.json';
    if (!window.languageSelectionSetting) {
        fetch(languageSettingUrl, {
            cache: 'no-cache'
        })
            .then(response => response.json())
            .then(setting => {
                window.languageSelectionSetting = setting
                dropdownLangSelect(lang);
            })
            .then(() => {
                setLangSelectEvent(i18nUrl);
            })
            .catch(error => console.log("found error", error));
    } else {
        dropdownLangSelect(lang);
        setLangSelectEvent(i18nUrl);
    }
}

function dropdownLangSelect(lang) {
    if (window.languageSelectionSetting && window.languageSelectionSetting.languages) {
        const flagImageUrl = window.languageSelectionSetting["flag_img_url"];
        const imageAttribute = window.languageSelectionSetting["img_attribute"];
        const dpElementId = window.languageSelectionSetting["element_id"];
        const dpImageElementId = window.languageSelectionSetting["element_img_id"];
        if (!flagImageUrl || !imageAttribute || !dpElementId || !dpImageElementId) {
            console.log("not found require data for language selection setting");
            return;
        }

        for (const key in window.languageSelectionSetting.languages) {
            const choiceElementId = window.languageSelectionSetting.languages[key]["element_id"];
            const imageSpritesCode = window.languageSelectionSetting.languages[key]["img_sprites_code"];
            const choiceImageElementId = window.languageSelectionSetting.languages[key]["element_img_id"];
            const dpImageElement = document.getElementById(dpImageElementId);
            const choiceElement = document.getElementById(choiceElementId);
            if (!choiceElementId || !imageSpritesCode || !choiceImageElementId || !dpImageElement || !choiceElement) {
                console.log("not found require data for language selection choice setting");
                return;
            }
            if (lang === key) {
                dpImageElement.setAttribute(imageAttribute, `${flagImageUrl}#${imageSpritesCode}`)
                choiceElement.classList.add("active");
                createCookie("current_language", key, 30);
            } else {
                choiceElement.classList.remove("active")
            }
        }
    }
}

function setLangSelectEvent(i18nUrl) {
    if (window.languageSelectionSetting && window.languageSelectionSetting.languages) {
        for (const key in window.languageSelectionSetting.languages) {
            const choiceElementId = window.languageSelectionSetting.languages[key]["element_id"];
            const choiceElement = document.getElementById(choiceElementId);
            if (!choiceElementId || !choiceElementId || !choiceElement) {
                console.log("not found require data for event binding");
                return;
            }
            choiceElement.addEventListener("click", event => {
                event.preventDefault();
                dropdownLangSelect(key)
                fetchI18nLabel(i18nUrl, key);
            });
        }
    }
}

function createCookie(name, value, days) {
    let expires;
    if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        let c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            let c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}